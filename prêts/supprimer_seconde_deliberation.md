http://www.assemblee-nationale.fr/15/amendements/0911/CION_LOIS/CL937.asp



ARTICLE ADDITIONNEL

AVANT L'ARTICLE PREMIER, insérer l'article suivant:

Le second alinéa de l’article 10 de la Constitution est supprimé.

EXPOSÉ SOMMAIRE

Les propositions que nous portons dans le cadre de la réforme constitutionnelle visent à renforcer les pouvoirs du Parlement et de l'opposition face à l'hypertrophie du pouvoir exécutif. Mettre fin à ce déséquilibre des pouvoirs est indispensable pour corriger le déficit démocratique du régime. Dans cet esprit, nous proposons également de renforcer les droits de participation démocratique. Enfin, une réforme de la Constitution ne peut se concevoir sans y inscrire des principes essentiels aujourd'hui absents de notre Loi fondamentale.

Dans ce cadre, les auteurs de cet amendement suppriment la possibilité pour le Président de la République de demander une seconde délibération sur les lois définitivement adoptées par le Parlement.

