# Supprimer la possibilité de dissolution du parlement par le Président

## Exposé des motifs

[voir:enlever_des_pouvoirs_au_président.md]

## Contenu

L'Article 12 de la Constitution est supprimé.
