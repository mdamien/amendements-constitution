https://fr.wikipedia.org/wiki/Imposition_en_France#Co%C3%BBt_de_gestion

Le nombre très élevé des impôts et taxes (214 taxes différentes en 2006) et leur complexité rendent le système fiscal français très difficile à gérer. Les coûts de gestion d'un certain nombre de taxes peuvent être supérieurs à ce qu'elles rapportent[réf. nécessaire]. Un rapport de l'Inspection générale des finances (IGF) de 2014, commandé par le gouvernement français, a identifié 192 « petites taxes », dont le rendement est inférieur à 150 millions d'euros par an36.
