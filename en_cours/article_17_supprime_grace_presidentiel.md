# Supprime la grace présidentielle

## Exposé des motifs

[voir:enlever_des_pouvoirs_au_président.md]

## Contenu

L'article 17 de la Constitution est supprimé

## Notes

Trouver bon exemple de mauvaise utilisation ou transferer ce pouvoir au Parlement