# Idées d'amendements la réforme constitutionelle en cours

[Voir dossier sur le site de l'assemblée](http://www.assemblee-nationale.fr/dyn/15/dossiers/democratie_plus_representative_responsable_efficace)

## Inspiration

 - [Amendements sur le PJL Numérique de 2015 pour la structure](https://github.com/regardscitoyens/pjlnum/blob/master/assemblee_amendements_commission/20150107_PJLNum_AmendementsCommission.pdf)
 - Viser la [république parlementaire](https://fr.wikipedia.org/wiki/Régime_parlementaire), comme la majorité des pays d'Europe
 - [Amendements citoyens proposés pour le Sénat](https://parlement-et-citoyens.fr/project/mettre-a-jour-notre-constitution-5-chapitres-pour-batir-une-nouvelle-democratie/synthesis/synthese-19)
 - Aller voir d'autres réformes constit et les [travaux du groupe de travail](http://www2.assemblee-nationale.fr/static/reforme-an/Rapport-1-GT.pdf)
    - Reprendre les amendements des députés LREM sur la derniére réforme de 2008

## Idées pour ce projet

 - Poser sur des articles existants plutot que de proposer un article additionel
 - Proposer aux députés de deposer des amendements identiques à des amendements posés par d'autres
 - Outil utile: Scipt pour générer du HTML/PDF des amendements pour faciliter la diffusion
 - Pour la suite: Site internet d'amendements citoyens où l'on peut approuver des amendements éxistants
 - A lire: http://www.conseil-constitutionnel.fr/conseil-constitutionnel/francais/documentation-publications/dossiers-thematiques/2008-cinquantenaire-la-constitution-en-20-questions/didier-maus.25795.html